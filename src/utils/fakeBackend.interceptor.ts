import { Injectable } from '@angular/core';
import { HTTP_INTERCEPTORS, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { delay, dematerialize, materialize, mergeMap } from 'rxjs/operators';

let users = JSON.parse(localStorage.getItem('users')) || [];

@Injectable()
export class FakeBackendInterceptor implements HttpInterceptor {
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const {url, method, headers, body} = request;
    console.log('request', url, method, headers, body)
    const getUsers = () => {
      return ok({users});
    };

    const getUserById = () => {
      const user = users.find(x => x.id === idFromUrl());
      return ok({user});
    };

    const deleteUser = () => {
      users = users.filter(x => x.id !== idFromUrl());
      localStorage.setItem('users', JSON.stringify(users));
      return ok();
    };

    const updateUser = () => {
      const user = JSON.parse(body);
      const userIndex = users.findIndex(x => x.id === idFromUrl());
      if (users[userIndex]) {
        user.id = idFromUrl();
        users[userIndex] = user;
        localStorage.setItem('users', JSON.stringify(users));
      }

      localStorage.setItem('users', JSON.stringify(users));
      return ok({user: users[userIndex]});
    };

    const addUser = () => {

      const user = JSON.parse(body);

      if (users.find(x => x.email === user.email)) {
        return error('Email "' + user.email + '" is already taken');
      }

      user.id = users.length ? Math.max(...users.map(x => x.id)) + 1 : 1;

      users.push(user);
      localStorage.setItem('users', JSON.stringify(users));

      return ok({user});
    };

    const ok = (data?) => {
      return of(new HttpResponse({status: 200, body: {success: true, data}}));
    };

    const error = (message) => {
      return throwError({error: {message}});
    };

    const idFromUrl = () => {
      const urlParts = url.split('/');
      return parseInt(urlParts[urlParts.length - 1], 10);
    };
    const handleRoute = () => {
      switch (true) {
        case url.endsWith('/users/add') && method === 'POST':
          return addUser();
        case url.endsWith('/users') && method === 'GET':
          return getUsers();
        case url.match(/\/users\/\d+$/) && method === 'GET':
          return getUserById();
        case url.match(/\/users\/\d+$/) && method === 'DELETE':
          return deleteUser();
        case url.match(/\/users\/\d+$/) && ['PUT', 'PATCH'].includes(method):
          return updateUser();
      }
      return next.handle(request);
    };
    return of(null)
      .pipe(mergeMap(handleRoute))
      .pipe(materialize())
      .pipe(delay(500))
      .pipe(dematerialize());
  }
}

export const FAKE_BACKEND_INTERCEPTORS = {
  // use fake backend in place of Http service for backend-less development
  provide: HTTP_INTERCEPTORS,
  useClass: FakeBackendInterceptor,
  multi: true
};
