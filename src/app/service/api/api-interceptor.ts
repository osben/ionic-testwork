import { Injectable } from '@angular/core';
import { HTTP_INTERCEPTORS, HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})

export class ApiInterceptor implements HttpInterceptor {
  headers = {
    'Content-Type': 'application/json'
  };


  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (request.url.includes(environment.API_ENDPOINT)) {
      const headersConfig = this.headers;

      const requestClone = request.clone({
        setHeaders: headersConfig,
      });

      return next.handle(requestClone)
        .pipe(
          retry(1),
          catchError((error: HttpErrorResponse) => {
            return throwError(error);
          }));
    }

    return next.handle(request);
  }
}

export const API_HTTP_INTERCEPTOR = [
  {provide: HTTP_INTERCEPTORS, useClass: ApiInterceptor, multi: true}
];
