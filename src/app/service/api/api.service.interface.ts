export interface RestResponseJsonSuccess<T = {}> {
  success: true;
  data: T;
}

export interface RestResponseJsonError<T = { message: string }> {
  success: false;
  data: T;
}

export type RestResponseData<T> = RestResponseJsonSuccess<T> | RestResponseJsonError;

export function isRestSuccess(success: boolean) {
  return success === true;
}

export declare function isRestResponseJsonSuccess<T>(value: RestResponseData<T>): value is RestResponseJsonSuccess<T>;

export declare function isRestResponseJsonError<T>(value: RestResponseData<T>): value is RestResponseJsonError;
