import { Injectable } from '@angular/core';
import { RestResponseData, RestResponseJsonError } from './api.service.interface';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  constructor(private http: HttpClient) {
  }

  get<Data>(path: string, params?: HttpParams | { [param: string]: string | string[] }) {
    return this.http.get<RestResponseData<Data>>(`${environment.API_ENDPOINT}${path}`, {...this.httpOptions, params})
      .pipe(
        catchError(this.formatErrors)
      );
  }

  post<Data>(path: string, body: any | null) {
    return this.http.post<RestResponseData<Data>>(`${environment.API_ENDPOINT}${path}`, JSON.stringify(body))
      .pipe(
        catchError(this.formatErrors)
      );
  }

  patch<Data>(path: string, body: any | null) {
    return this.http.patch<RestResponseData<Data>>(`${environment.API_ENDPOINT}${path}`, JSON.stringify(body))
      .pipe(
        catchError(this.formatErrors)
      );
  }

  delete<Data>(path: string) {
    return this.http.delete<RestResponseData<Data>>(`${environment.API_ENDPOINT}${path}`)
      .pipe(
        catchError(this.formatErrors)
      );
  }

  formatErrors(error: any): Observable<RestResponseJsonError> {
    let errorMessage: string;
    if (error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    return of({success: false, data: {message: errorMessage || 'error'}}) as Observable<RestResponseJsonError>;
  }
}
