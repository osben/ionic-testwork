import { Component, OnInit } from '@angular/core';
import { UserProfile } from '../../models/user-profile';
import { AlertController, LoadingController, ModalController } from '@ionic/angular';
import { roleEditProfile, UserProfilePage } from '../user-profile/user-profile.page';
import { ApiService } from '../../service/api/api.service';


@Component({
  selector: 'app-users',
  templateUrl: './users.page.html',
  styleUrls: ['./users.page.scss'],
})
export class UsersPage implements OnInit {
  public users: UserProfile[] = [];

  constructor(private api: ApiService,
              private modalController: ModalController,
              private loadingController: LoadingController,
              private alertController: AlertController) {
  }

  ngOnInit() {
    this.fetchUsers();
  }

  private fetchUsers() {
    this.users = undefined;
    this.api.get<{ users: UserProfile[] }>('users')
      .subscribe(res => {

        if (res.success === true) {
          this.users = res.data.users;
        }
      });
  }


  public async editOrAddUser(user?: UserProfile) {
    const modal = await this.modalController.create({
      component: UserProfilePage,
      cssClass: 'user-profile-page',
      componentProps: {
        user
      }
    });
    await modal.present();
    const {data, role} = await modal.onWillDismiss<{ user?: UserProfile }>();
    switch (role) {
      case roleEditProfile.ADD:
      case roleEditProfile.EDIT:
        this.fetchUsers();
        break;
    }
  }

  /**
   * Is delete user ?
   */
  public async deleteUserConfirm(user: UserProfile) {

    let isConfirmWait = false;
    const alert = await this.alertController.create({
      header: 'Are you sure ?',
      backdropDismiss: false,
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {

          }
        },
        {
          text: 'Confirm',
          handler: async () => {
            if (isConfirmWait) {
              return false;
            }
            isConfirmWait = true;
            const loading = await this.loadingController.create();
            await loading.present();
            this.api.delete(`users/${user.id}`)
              .subscribe(res => {
                this.fetchUsers();
                alert.dismiss();
                loading.dismiss();
              });
            return false;
          }
        }
      ]
    });
    await alert.present();
  }
}
