import { Component, Input, OnInit } from '@angular/core';
import { AlertController, LoadingController, ModalController, ToastController } from '@ionic/angular';
import { UserProfile } from '../../models/user-profile';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ApiService } from '../../service/api/api.service';

export enum roleEditProfile {
  CLOSE = 'close',
  EDIT = 'edit',
  ADD = 'add'
}

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.page.html',
  styleUrls: ['./user-profile.page.scss'],
})
export class UserProfilePage implements OnInit {

  @Input() user?: UserProfile;
  userForm: FormGroup;
  isSubmitted = false;

  constructor(private api: ApiService,
              private modalCtrl: ModalController,
              private alertController: AlertController,
              private toastController: ToastController,
              private loadingController: LoadingController,
              private fb: FormBuilder) {
  }

  ngOnInit() {
    this.constructForm();
  }

  private constructForm() {
    this.userForm = this.fb.group({
      name: [this.user?.name || '', [Validators.required, Validators.minLength(2)]],
      email: [this.user?.email || '', [Validators.required, Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$')]]
    });
  }

  get errorControl() {
    return this.userForm.controls;
  }

  async submitForm() {
    // invalid or double submit
    if (!this.userForm.valid || this.isSubmitted) {
      return;
    }
    this.isSubmitted = true;
    const isEditUser: boolean = !!(this.user?.id);
    const loading = await this.loadingController.create();
    await loading.present();
    (isEditUser ?
        this.api.patch<{ user: UserProfile }>(`users/${this.user.id}`, {...this.userForm.value}) :
        this.api.post<{ user: UserProfile }>('users/add', {...this.userForm.value})
    ).subscribe(async (res) => {
      // TODO hide spinner
      if (res.success === false) {
        const alert = await this.alertController.create({
          message: res.data.message || 'Error',
          backdropDismiss: false,
          buttons: [
            {
              text: 'Ok'
            }
          ]
        });
        await alert.present();
      } else {
        const toast = await this.toastController.create({
          message: isEditUser ? 'User edit successfully' : 'User added successfully',
          color: 'success',
          duration: 2000
        });
        await toast.present();
        await this.closeModal(res.data.user, isEditUser ? roleEditProfile.EDIT : roleEditProfile.ADD);
      }
      await loading.dismiss();
      this.isSubmitted = false;
    });

  }

  async closeModal(data = {}, role = roleEditProfile.CLOSE) {
    return await this.modalCtrl.dismiss(data, roleEditProfile.CLOSE);
  }
}
